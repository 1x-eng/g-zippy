# Zippy CORE powered by serverless framework + ExpressJS with Typescript.

### Features

Developer experience first:

- 🔥 [Serverless framework](https://www.serverless.com)
- 📖 Local support with Serverless Offline
- ⚙️ Environment variable with Serverless Dotenv
- ⚡️ [ExpressJS](http://expressjs.com)
- 🎉 Type checking [TypeScript](https://www.typescriptlang.org) with strict mode
- ✏️ Linter with [ESLint](https://eslint.org) with Airbnb configuration
- 🛠 Code Formatter with [Prettier](https://prettier.io)
- 🚫 Lint-staged for running linters on Git staged files
- 🗂 VSCode configuration: Debug, Settings, Tasks and extension for ESLint, Prettier, TypeScript
- ✨ HTTP Api instead of API gateway for cost optimization
- 💨 Live reload

### Philosophy

- Minimal code
- 🚀 Production-ready

### Requirements

- Node.js and npm

### Getting started

Run the following command on your local environment:

```
git clone https://gitlab.com/1x-eng/g-zippy.git
cd g-zippy/core
npm install
```

Then, you can run locally in development mode with live reload:

```
npm run dev
```

The local server is now listening at http://localhost:3009

### Deploy to production

Zippy is production ready for "AWS" exclusively. Hence, there is a pre-requisite to have an [AWS user creds](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) available in your local/deployment machine's - `~/.aws/credentials`.
Additionally, please assign a name for your `profile`. (Expected name = `zippy`. If you name your profile anything else, ensure to reflect those changes into package.json for `deploy-prod` and `remove-prod` steps.) 

You can deploy to production with the following command:

```
npm run deploy-prod
```

### Delete production stack
You may also destroy your zippy stack entirely from your remote AWS account. 

```
npm run remove-prod
```
PS: This will irrevocably delete everything in relation to zippy; there is no scope for recovery.

### Things to know

`serverless-offline-plugin` display a red warning in the console `offline: [object Object]`. It's just a warning from [Serverless Offline Plugin](https://github.com/dherault/serverless-offline/blob/b39e8cf23592ad8bca568566e10c3db3469a951b/src/utils/getHttpApiCorsConfig.js). Hope it'll solve in the next release of `serverless-offline-plugin`.

### VSCode information (optional)

If you are VSCode users, you can have a better integration with VSCode by installing the suggested extension in `.vscode/extension.json`. The starter code comes up with Settings for a seamless integration with VSCode. The Debug configuration is also provided for frontend and backend debugging experience.

Pro tips: if you need a project wide type checking with TypeScript, you can run a build with <kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>B</kbd> on Mac.

### Contributions

Everyone is welcome to contribute to this project. Feel free to open an issue if you have question or found a bug.

### License

Licensed under the MIT License, Copyright © 2022

See [LICENSE](LICENSE) for more information.

---

Product of [Adroit](https://adroitcorp.com.au)