import { Router } from 'express';

import userController from '../controllers/user';
import authMiddleware from '../middlewares/auth';

const privateRouter = Router();

privateRouter.get('/user/profile', authMiddleware, userController.getProfile);

export default privateRouter;
