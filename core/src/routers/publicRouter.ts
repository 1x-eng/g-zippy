import express, { Router } from 'express';

import userController from '../controllers/user';
import privateRouter from './privateRouter';

interface IRoutesController {
  routes: express.Handler;
}

const publicRouter = Router();

const routesController: IRoutesController = {
  routes: async (_req, res) => {
    const publicRoutes: any[] = [];
    const privateRoutes: any[] = [];

    publicRouter.stack.forEach(function (middleware) {
      if (middleware.route) {
        // routes registered directly on the app
        publicRoutes.push(middleware.route);
      } else if (middleware.name === 'router') {
        // router middleware
        middleware.handle.stack.forEach(function (handler: { route: any }) {
          const { route } = handler;
          route && publicRoutes.push(route);
        });
      }
    });

    privateRouter.stack.forEach(function (middleware) {
      if (middleware.route) {
        // routes registered directly on the app
        privateRoutes.push(middleware.route);
      } else if (middleware.name === 'router') {
        // router middleware
        middleware.handle.stack.forEach(function (handler: { route: any }) {
          const { route } = handler;
          route && privateRoutes.push(route);
        });
      }
    });

    res.json({
      public: publicRoutes,
      private: privateRoutes,
    });
  },
};

publicRouter.get('/registry', routesController.routes);
publicRouter.post('/signup', userController.signUp);
publicRouter.post('/signup/confirm', userController.confirmSignUp);
publicRouter.post('/signin', userController.signIn);
publicRouter.post('/forgotpassword', userController.forgotPassword);
publicRouter.post('/confirmpassword', userController.confirmPassword);
publicRouter.post('/signout', userController.signOut);

export default publicRouter;
