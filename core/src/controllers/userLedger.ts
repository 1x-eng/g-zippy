import { Lambda } from 'aws-sdk';

const lambda = new Lambda();
const userLedgerLambdaInvokeParams = {
  FunctionName: `zippy-core-${process.env.ENV}-user-ledger-handler`,
  InvocationType: 'Event',
  LogType: 'Tail',
};

interface IUserLedgerController {
  recordToUserLedger: (
    email: string,
    activityDateTime: Date,
    activityType: ActivityType
  ) => void;
}

export const enum ActivityType {
  signup,
  signIn,
  confirmSignUp,
  forgotPassword,
  confirmPassword,
  signOut,
  getProfile,
}

export const userLedgerController: IUserLedgerController = {
  recordToUserLedger: (
    email: string,
    activityDateTime: Date,
    activityType: ActivityType
  ) => {
    const message = () => {
      if (activityType == ActivityType.signup) {
        return 'Signup successful. User created in cognito userpool. (not yet confirmed)';
      }
      if (activityType == ActivityType.signIn) {
        return 'Signin successful.';
      }
      if (activityType == ActivityType.confirmSignUp) {
        return 'User has confirmed their signup (either through an admin or passcode sent via email)';
      }
      if (activityType == ActivityType.forgotPassword) {
        return 'User has initiated forgot password workflow';
      }
      if (activityType == ActivityType.confirmPassword) {
        return 'User has confirmed password; after initiating forgot password workflow';
      }
      if (activityType == ActivityType.signOut) {
        return 'User has signed out';
      }

      return 'User profile information (metadata / attributes associated with user object) returned to caller.';
    };
    const activityDateTimeISOString = activityDateTime.toISOString();

    const payload_obj = {
      Payload: JSON.stringify({
        email,
        activityDateTimeISOString,
        message: message(),
        type: "put"
      }),
    };
    const userLedgerLambdaPayload = {
      ...userLedgerLambdaInvokeParams,
      ...payload_obj,
    };

    lambda.invoke(userLedgerLambdaPayload, (err, lambdaRes) => {
      if (err) {
        throw Error(
          `User ledger handler invocation unsuccessful. Error details = ${err.message}`
        );
      } else {
        console.log(
          `User ledger handler invocation successful. Response = `, lambdaRes
        );
      }
    });
  },
};
