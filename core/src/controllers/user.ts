import express from 'express';

import { IAuthenticatedRequest } from '../middlewares/auth';
import cognitoUserPoolHelper from '../services/cognitoUserPool';
import { ActivityType, userLedgerController } from './userLedger';

interface IUserController {
  signUp: express.Handler;
  signIn: express.Handler;
  confirmSignUp: express.Handler;
  forgotPassword: express.Handler;
  confirmPassword: express.Handler;
  signOut: express.Handler;
  getProfile: express.Handler;
}

const userController: IUserController = {
  signUp: async (req, res) => {
    try {
      const {
        email,
        password,
        address,
        birthdate,
        family_name,
        gender,
        given_name,
        locale,
        middle_name,
        name,
        nickname,
        phone_number,
        picture,
        preferred_username,
        profile,
        updated_at,
        website,
        zoneinfo,
      } = req.body;
      const result = await cognitoUserPoolHelper.signUp({
        email,
        password,
        address,
        birthdate,
        family_name,
        gender,
        given_name,
        locale,
        middle_name,
        name,
        nickname,
        phone_number,
        picture,
        preferred_username,
        profile,
        updated_at,
        website,
        zoneinfo,
      });
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.signup
      );
      res.json({ message: `${result} is created.` });
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  confirmSignUp: async (req, res) => {
    try {
      const { email, code } = req.body;
      const result = await cognitoUserPoolHelper.confirmSignUp({ email, code });
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.confirmSignUp
      );
      res.json({ message: result });
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  signIn: async (req, res) => {
    try {
      const { email, password } = req.body;
      const result = await cognitoUserPoolHelper.signIn({ email, password });
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.signIn
      );
      res.json(result);
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  forgotPassword: async (req, res) => {
    try {
      const { email } = req.body;
      const result = await cognitoUserPoolHelper.forgotPassword(email);
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.forgotPassword
      );
      res.json({ message: result });
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  confirmPassword: async (req, res) => {
    try {
      const { email, verificationCode, newPassword } = req.body;
      const result = await cognitoUserPoolHelper.confirmPassword({
        email,
        verificationCode,
        newPassword,
      });
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.confirmPassword
      );
      res.json({ message: result });
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  signOut: async (req, res) => {
    try {
      const { email } = req.body;
      const result = await cognitoUserPoolHelper.signOut(email);
      userLedgerController.recordToUserLedger(
        email,
        new Date(),
        ActivityType.signOut
      );
      res.json({ message: result });
    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  getProfile: (req: IAuthenticatedRequest, res) => {
    userLedgerController.recordToUserLedger(
      req.user?.email || 'unknown',
      new Date(),
      ActivityType.getProfile
    );
    res.json(req.user);
  },
};

export default userController;
