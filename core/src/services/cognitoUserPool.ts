import 'cross-fetch/polyfill';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
} from 'amazon-cognito-identity-js';

export interface IUserToken {
  accessToken: string;
  refreshToken: string;
}

interface IUserSignupPayload {
  email: string;
  password: string;
  address: JSON;
  birthdate: string;
  family_name: string;
  gender: string;
  given_name: string;
  locale: string;
  middle_name: string;
  name: string;
  nickname: string;
  phone_number: string;
  picture: string;
  preferred_username: string;
  profile: string;
  updated_at: number;
  website: string;
  zoneinfo: string;
}

class CognitoUserPoolService {
  public userPool: CognitoUserPool;

  constructor() {
    this.userPool = new CognitoUserPool({
      UserPoolId: process.env.USER_POOL_ID || '',
      ClientId: process.env.CLIENT_ID || '',
    });
  }

  public signUp({
    email,
    password,
    address,
    birthdate,
    family_name,
    gender,
    given_name,
    locale,
    middle_name,
    name,
    nickname,
    phone_number,
    picture,
    preferred_username,
    profile,
    updated_at,
    website,
    zoneinfo,
  }: IUserSignupPayload): Promise<string> {
    return new Promise((resolve, reject) => {
      const attributeList: CognitoUserAttribute[] = [
        new CognitoUserAttribute({
          Name: 'email',
          Value: email,
        }),
        new CognitoUserAttribute({
          Name: 'address',
          Value: address ? JSON.stringify(address) : '',
        }),
        new CognitoUserAttribute({
          Name: 'birthdate',
          Value: birthdate,
        }),
        new CognitoUserAttribute({
          Name: 'family_name',
          Value: family_name,
        }),
        new CognitoUserAttribute({
          Name: 'gender',
          Value: gender,
        }),
        new CognitoUserAttribute({
          Name: 'given_name',
          Value: given_name,
        }),
        new CognitoUserAttribute({
          Name: 'locale',
          Value: locale,
        }),
        new CognitoUserAttribute({
          Name: 'middle_name',
          Value: middle_name,
        }),
        new CognitoUserAttribute({
          Name: 'name',
          Value: name,
        }),
        new CognitoUserAttribute({
          Name: 'nickname',
          Value: nickname,
        }),
        new CognitoUserAttribute({
          Name: 'phone_number',
          Value: phone_number,
        }),
        new CognitoUserAttribute({
          Name: 'picture',
          Value: picture,
        }),
        new CognitoUserAttribute({
          Name: 'preferred_username',
          Value: preferred_username,
        }),
        new CognitoUserAttribute({
          Name: 'profile',
          Value: profile,
        }),
        new CognitoUserAttribute({
          Name: 'updated_at',
          Value: updated_at?.toString(),
        }),
        new CognitoUserAttribute({
          Name: 'website',
          Value: website,
        }),
        new CognitoUserAttribute({
          Name: 'zoneinfo',
          Value: zoneinfo,
        }),
      ];

      this.userPool.signUp(
        email,
        password,
        attributeList,
        [],
        (err, result) => {
          if (err) {
            return reject(err);
          }

          resolve(result?.user.getUsername() || '');
        }
      );
    });
  }

  public confirmSignUp({
    email,
    code,
  }: {
    email: string;
    code: string;
  }): Promise<string> {
    return new Promise((resolve, reject) => {
      const cognitoUser = new CognitoUser({
        Username: email,
        Pool: this.userPool,
      });

      cognitoUser.confirmRegistration(code, true, (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result);
      });
    });
  }

  public signIn({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Promise<IUserToken | { userConfirmationNecessary: boolean }> {
    return new Promise((resolve, reject) => {
      const cognitoUser = new CognitoUser({
        Username: email,
        Pool: this.userPool,
      });

      const authenticationDetails = new AuthenticationDetails({
        Username: email,
        Password: password,
      });
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (session, userConfirmationNecessary) => {
          if (userConfirmationNecessary) {
            return resolve({ userConfirmationNecessary });
          }

          resolve({
            accessToken: session.getAccessToken().getJwtToken(),
            refreshToken: session.getRefreshToken().getToken(),
          });
        },
        onFailure: (err) => {
          reject(err);
        },
      });
    });
  }

  public forgotPassword(email: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const cognitoUser = new CognitoUser({
        Username: email,
        Pool: this.userPool,
      });

      cognitoUser.forgotPassword({
        onSuccess(data) {
          // successfully initiated reset password request
          console.log(
            `CodeDeliveryData from forgotPassword:${data}, User: ${email}`,
            data
          );
          resolve(
            `Password reset code has been sent to ${data.CodeDeliveryDetails.Destination} via ${data.CodeDeliveryDetails.DeliveryMedium}`
          );
        },
        onFailure(err) {
          console.error(`Forgot password failed for ${email} due to ${err}`);
          return reject(err);
        },
      });
    });
  }

  public confirmPassword({
    email,
    verificationCode,
    newPassword,
  }: {
    email: string;
    verificationCode: string;
    newPassword: string;
  }): Promise<string> {
    return new Promise((resolve, reject) => {
      const cognitoUser = new CognitoUser({
        Username: email,
        Pool: this.userPool,
      });

      cognitoUser.confirmPassword(verificationCode, newPassword, {
        onSuccess() {
          console.log(
            `Password confirmed!, Password reset complete for ${email}`
          );
          resolve(`Password reset successful!`);
        },
        onFailure(err) {
          console.error(
            `Password confirmation failed for ${email} due to ${err}`
          );
          return reject(err);
        },
      });
    });
  }

  public signOut(email: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const cognitoUser = new CognitoUser({
        Username: email,
        Pool: this.userPool,
      });

      try {
        cognitoUser.signOut();
        console.log(`User - ${email} signed out successfully.`);
        resolve(`Signout successful!`);
      } catch (err) {
        console.error(`Signout failed for ${email} due to ${err}`);
        return reject(err);
      }

      // Should one need global signout; use below implementation instead.

      // cognitoUser.globalSignOut({
      //     onSuccess(){
      //         console.log(`User - ${email} signed out globally.`);
      //         resolve(`Signout successful!`);
      //     },
      //     onFailure(err){
      //         console.error(`Signout failed for ${email} due to ${err}`);
      //         return reject(err);
      //     }
      // });
    });
  }
}

export default new CognitoUserPoolService();
