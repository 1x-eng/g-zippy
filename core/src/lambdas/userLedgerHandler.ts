import { DynamoDB } from 'aws-sdk';

const dynamoDB = new DynamoDB.DocumentClient();
const userLedgerTableName = process.env.LEDGER_TABLE_NAME || "zippy-users-ledger";


const userLedgerHandler = {
  put: async (event: { email: string, activityDateTimeISOString: string, message: string }, _: any) => {

    try{
    
      await dynamoDB.put({
        TableName: userLedgerTableName,
        Item: {
          email: event.email,
          activityDateTime: event.activityDateTimeISOString,
          message: event.message
        }
      }).promise();

      console.log(`User activity captured into users ledger successfully.`);
    
    } catch (err: unknown) {
      if (err instanceof Error) {
        console.error(`Unable to record into user ledger. Details = ${err.message}`);
      }
      throw Error(
        `Unable to capture user activity into ledger.`
      );
    }
  },
  get: async (event: {email: string}, _: any) => {
    
    try{
    
      return await dynamoDB.get({
        TableName: userLedgerTableName,
        Key: {
          email: event.email
        }
      }).promise();

    } catch (err: unknown) {
      if (err instanceof Error) {
        console.error(`Unable to record into user ledger. Details = ${err.message}`);
      }
      throw Error(
        `User ledger retrieval unsuccessful.`
      );
    }
  }
}

export const handler = async function (event: {email: string, activityDateTimeISOString: string, message: string, type: string }, context: any) {

  if (event.type == "put") {
    return userLedgerHandler.put({email: event.email, activityDateTimeISOString: event.activityDateTimeISOString, message: event.message}, context)
  }
  else if (event.type == "get") {
    return userLedgerHandler.get({email: event.email}, context);
  }
  else {
    throw Error(`Unknown ledger handler type. Only put & get ops are permitted.`);
  }
  
};
