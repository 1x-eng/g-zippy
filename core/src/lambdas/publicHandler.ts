import express, { json } from 'express';
import helmet from 'helmet';
import serverlessHttp from 'serverless-http';

import publicRouter from '../routers/publicRouter';
import apiVersion from '../utilities/version';

const app = express();
app.use(json());
app.use(helmet());

app.use(`/pl/${apiVersion}`, publicRouter);

app.use((_, res, _2) => {
  res.status(404).json({ error: 'NOT FOUND' });
});

export const handler = serverlessHttp(app);
