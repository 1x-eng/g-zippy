const apiVersion = process.env.API_VERSION || 'v1';

export default apiVersion;
