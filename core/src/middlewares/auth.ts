import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { Handler, Request } from 'express';

const identityServiceProvider = new CognitoIdentityServiceProvider({
  region: process.env.REGION || 'ap-southeast-2',
});

export interface IUser {
  id: string;
  email: string;
  attributes: CognitoIdentityServiceProvider.AttributeListType;
}

export interface IAuthenticatedRequest extends Request {
  user?: IUser;
}

const authMiddleware: Handler = async (
  req: IAuthenticatedRequest,
  _res,
  next
) => {
  try {
    const token = req.headers.authorization!;
    const rawUser = await identityServiceProvider
      .getUser({ AccessToken: token })
      .promise();
    req.user = {
      id: rawUser.UserAttributes.find((attr) => attr.Name === 'sub')?.Value!,
      email: rawUser.UserAttributes.find((attr) => attr.Name === 'email')
        ?.Value!,
      attributes: rawUser.UserAttributes,
    };
    next();
  } catch (err) {
    next(err);
  }
};

export default authMiddleware;
