variable "env" {}
variable "country" {}
variable "region_of_operation" {}

# Default tags
variable "default_tags" {
  type = map(any)
  default = {
    created_by  = "terraform",
    managed_by  = "terraform",
    owner       = "terraform/admin"
    project     = "zippy"
    description = "Created or generated as part of zippy tech stack and is controlled by terraform."
  }
}

variable "create_iam_for_zippy_lambda_workers" {
  default = true
}
variable "zippy_lambda_workers_component_name" {
  default = "gzippy_lambda"
}

#################################################################################################
# ElasticSearch with KMS encryption
#################################################################################################

variable "create_es" {
  default = true # AWS OpenSearch is not serverless; be aware of the cost implications.
}
variable "es_whitelist_ip_address_with_range" {
  default = "0.0.0.0/0"
}
variable "es_kms_description" {
  default = "KMS key to encrypt the Elasticsearch volume"
}
variable "es_kms_key_name" {
  default = "alias/elasticsearch-kms"
}

variable "es_domain_name" {
  default = "zippy-es-single-node"
}
variable "es_version" {
  default = "7.10"
}
variable "es_master_username" {}

#################################################################################################
# RDS SLS Aurora PG
#################################################################################################
variable "create_rds_aurora_sls_pg" {
  default = false
}
variable "rds_aurora_sls_pg_master_username" {}
