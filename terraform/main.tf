#################################################################################################
# IAM for zippy workers.
#################################################################################################
module "zippy_iam_for_lambda_workers" {
  source = "./modules/iam"

  create        = var.create_iam_for_zippy_lambda_workers
  env           = var.env
  country_code  = var.country
  aws_component = var.zippy_lambda_workers_component_name
  default_tags  = var.default_tags
}

#################################################################################################
# RDS Aurora SLS Postgres
#################################################################################################
module "zippy_rds_aurora_sls_postgres" {
  source = "./modules/rds_aurora_sls_pg"

  create_rds_aurora_sls_pg = var.create_rds_aurora_sls_pg
  env                      = var.env
  country_code             = var.country
  region_of_operation      = var.region_of_operation
  master_username          = var.rds_aurora_sls_pg_master_username
  default_tags             = var.default_tags
}

#################################################################################################
# ElasticSearch with KMS encryption
#################################################################################################

module "zippy_es_kms" {
  source         = "./modules/kms"
  create         = var.create_es
  description    = var.es_kms_description
  alias_key_name = var.es_kms_key_name
  default_tags   = var.default_tags
}


module "zippy_elasticsearch" {
  source = "./modules/elasticsearch"

  create     = var.create_es
  aws_region = var.region_of_operation

  # Security
  whitelist_ip_address_with_range = var.es_whitelist_ip_address_with_range
  encryption_kms_key_id           = module.zippy_es_kms.zippy_kms_key_id

  # Elasticsearch
  domain_name           = var.es_domain_name
  elasticsearch_version = var.es_version
  es_master_username    = var.es_master_username

  # Tags
  default_tags = var.default_tags
}

module "zippy_elasticsearch_master_username" {
  source = "./modules/ssm_parameter_store"

  create                    = var.create_es
  env                       = var.env
  country_code              = var.country
  ssm_parameter_name        = "es_master_username"
  ssm_parameter_description = "ES master username"
  ssm_parameter_value       = var.es_master_username
  default_tags              = var.default_tags
}

module "zippy_elasticsearch_master_password" {
  source = "./modules/ssm_parameter_store"

  create                    = var.create_es
  env                       = var.env
  country_code              = var.country
  ssm_parameter_name        = "es_master_password"
  ssm_parameter_description = "ES master password"
  ssm_parameter_value       = join("", module.zippy_elasticsearch.*.es_master_password)
  default_tags              = var.default_tags
}

module "zippy_elasticsearch_endpoint" {
  source = "./modules/ssm_parameter_store"

  create                    = var.create_es
  env                       = var.env
  country_code              = var.country
  ssm_parameter_name        = "es_endpoint"
  ssm_parameter_description = "ES endpoint"
  ssm_parameter_value       = module.zippy_elasticsearch.elasticsearch_endpoint
  default_tags              = var.default_tags
}

module "zippy_kibana_endpoint" {
  source = "./modules/ssm_parameter_store"

  create                    = var.create_es
  env                       = var.env
  country_code              = var.country
  ssm_parameter_name        = "kibana_endpoint"
  ssm_parameter_description = "Kibana endpoint"
  ssm_parameter_value       = module.zippy_elasticsearch.elasticsearch_kibana_endpoint
  default_tags              = var.default_tags
}
