# Aurora PG Handler

The scope of this script is to help automate creation of tables in Aurora PG leveraging Aurora Data API to best extent possible. The script validates the incoming schema definition file (A csv provided by zippy user, uploaded into 'schemas' folder) and with successful validation, generates a CREATE DDL statement, uses RDS DATA API to interact with the PG Cluster and create the respective table. The

Pre-requisites:

1. Python 3.9+
2. boto3 package; there is a requirements.txt highlighting boto3 and its associated packages as a dependency.

# gzippy codegen

Script to generate golang code for lambda functions that enable CRUD ops over respective `table` in AuroraPG.

Pre-requisites:

1. Python 3.9+