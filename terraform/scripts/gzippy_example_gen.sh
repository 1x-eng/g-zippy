#! /bin/bash
echo -e "##########################################################################"
echo -e "# gzippy example generator starting for - '$1'"
echo -e "##########################################################################\n"

./gzippy_code_gen.sh $1 $2 $3 'n'
cd ../lambdas/$1/ && cat <<EOT >> main.go

package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
)

type Request struct {
	ID    float64 \`json:"id"\`
	Value string  \`json:"value"\`
}

type Response struct {
	Message string \`json:"message"\`
	Ok      bool   \`json:"ok"\`
}

func Handler(request Request) (Response, error) {
	return Response{
		Message: fmt.Sprintf("Process Request ID %f", request.ID),
		Ok:      true,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
EOT

cd ../../../core/src/controllers && touch gzippyExample.ts && cat <<EOT >> gzippyExample.ts
import express from 'express';
import { Lambda } from 'aws-sdk';
import { IAuthenticatedRequest } from '../middlewares/auth';

const lambda = new Lambda();

interface IGzippyExample {
  getHandler: express.Handler;
  postHandler: express.Handler;
}

const gzippyExampleController: IGzippyExample = {
  getHandler: async(req: IAuthenticatedRequest, res) => {
    try {
      const {
        id,
        value
      } = req.query;

      const params = {
        FunctionName: "zippy_$2_$3_$1",
        InvocationType: "RequestResponse",
        LogType: "Tail",
        Payload: JSON.stringify({
          id: parseFloat(<string>id),
          value
        })
      };

      lambda.invoke(params, (err, lambdaRes) => {
        if (err) {
          throw Error(err.message);
        } else {
          res.json({ message: JSON.parse(<string>lambdaRes.Payload) });
        }
      });

    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
  postHandler: async(req: IAuthenticatedRequest, res) => {
    try {
      const {
        id,
        value
      } = req.body;

      const params = {
        FunctionName: "zippy_$2_$3_$1",
        InvocationType: "RequestResponse",
        LogType: "Tail",
        Payload: JSON.stringify({
          id: parseFloat(<string>id),
          value
        })
      };

      lambda.invoke(params, (err, lambdaRes) => {
        if (err) {
          throw Error(err.message);
        } else {
          res.json({ message: JSON.parse(<string>lambdaRes.Payload) });
        }
      });

    } catch (err: unknown) {
      if (err instanceof Error) {
        res.status(500).json({ message: err.message });
      }
    }
  },
};

export default gzippyExampleController;
EOT

cd ../routers && cat <<EOT >> privateRouter.ts
/*
 * Auto Generated Code - For "$1"
 */
import gzippyExampleController from '../controllers/gzippyExample';

privateRouter.get('/gzippy/example', authMiddleware, gzippyExampleController.getHandler);
privateRouter.post('/gzippy/example', authMiddleware, gzippyExampleController.postHandler);

export default privateRouter;
EOT

sed -i '0,/export default privateRouter;/s/export default privateRouter;//' privateRouter.ts

echo -e "##########################################################################"
echo -e "# gzippy example generator completed for - '$1'"
echo -e "##########################################################################\n"