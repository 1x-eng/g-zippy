import argparse
import boto3
import csv
import os
import re
import string
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('--schema_file_name', help='Name of the schema file with extension. Should exist within "schemas" directory.')
parser.add_argument('--env', help='Env name. Eg. prod/staging/dev.')
parser.add_argument('--country_code', help='ISO country code of AWS region where the stack is deployed in.')
parser.add_argument('--create_table', help='y/n instructing auto DDL to either create table inferring schema or not.')

class AuroraPGSchemaFileValidator:
    """
    Validate Incoming Schema File; so DDL &/ DML can be safe over Aurora PG
    """
    
    def __init__(self, schema_file_name: str):
        self.schema_file_path = f"../schemas/{schema_file_name}" # This script is meant to be run from "terraform/scripts" directory.

    def _schema_file_exists_and_not_empty(self) -> bool:
        try:
            if os.path.exists(self.schema_file_path) and os.path.getsize(self.schema_file_path) > 0:
                return True
            print(f"""[Aurora PG Handler] {self.schema_file_path} does not exist under 'schemas' directory. Please do not forget, the file name SHOULD consist a prefix of 'aurora_pg_tbl_'. 
            file name should end with g-zippy package name (for eg. if package name passed to make gzippy/create is 'employee', then its associated schema file name SHOULD be 'aurora_pg_tbl_employee.csv')""")
            return False
        except Exception as e:
            print(f"[Aurora PG Handler] Unable to confirm if schema file exists. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Unable to confirm if schema file exists")

    def _validate_schema_file_extension(self) -> bool:
        try:
            if self.schema_file_path.endswith(".csv"):
                return True
            print("[Aurora PG Handler] Unsupported schema file extension.")
            return False
        except Exception as e:
            print(f"[Aurora PG Handler] Unable to validate schema file extension. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Unable to validate schema file extension")

    def _validate_schema_file_syntax(self) -> bool:
        try:
            reader = csv.reader(open(self.schema_file_path, "r"))
            for row in reader:
                if len(row) != 3:
                    print("[Aurora PG Handler] Schema file syntax is invalid. Every row is expected to be just like this - column name, column type, column constraint. (eg. first_name,varchar,PRIMARY KEY)")
                    return False
            return True
        except Exception as e:
            print(f"[Aurora PG Handler] Unable to validate schema file syntax. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Unable to validate schema file syntax")

    def _validate_col_name_type_and_constraints_in_schema_file(self) -> bool:
        # Types Reference: https://www.postgresql.org/docs/14/datatype.html
        valid_types = ["bigint", "int8", "bigserial", "serial8", "bit", "bit varying", "varbit", "boolean", "bool",
        "box", "bytea", "character", "char", "character varying", "varchar", "cidr", "circle", "date", 
        "double precision", "float8", "inet", "integer", "int", "int4", "interval", "json", "jsonb", "line", 
        "lseg", "macaddr", "macaddr8", "money", "numeric", "decimal", "path", "pg_lsn", "pg_snapshot", "point", 
        "polygon", "real", "float4", "smallint", "int2", "smallserial", "serial2", "serial", "serial4", "text",
        "time", "timetz", "timestamp", "timestamptz", "tsquery", "tsvector", "txid_snapshot", "uuid", "xml"]
        
        valid_constraints = ["", "NOT NULL", "UNIQUE", "UNIQUE NOT NULL", "NOT NULL UNIQUE", "PRIMARY KEY", "PRIMARY KEY NOT NULL", "REFERENCES", "CHECK"]

        schema_as_dict = {}
        try:
            with open(self.schema_file_path, "r") as s:
                reader = csv.reader(s)
                for row in reader:
                    # Read the first column of the csv as the key, then read the remaining columns as a list for the values
                    schema_as_dict[row[0]] = [row[x] for x in range (1, len (row))]

            for col in schema_as_dict:
                if not col.islower():
                    print("[Aurora PG Handler] column name is invalid. Only lower case string (in snake case) is a valid name.")
                    return False

                if " " in col:
                    print("[Aurora PG Handler] column name contains a space. Only lower case string (in snake case) is a valid name.")
                    return False

                if any(char.isdigit() for char in col):
                    print("[Aurora PG Handler] column name contains a number. Only lower case string (in snake case) is a valid name.")
                    return False

                if not re.match(r'^[A-Za-z_]+$', col):
                    print("[Aurora PG Handler] column name contains unaccepted character(s). Only lower case string (in snake case) is a valid name.")
                    return False   

                if schema_as_dict[col][0] not in valid_types:
                    # Strip any trailing digits (eg. varchar10) and check if type is valid
                    coltype_stripped_of_trailing_numbers = schema_as_dict[col][0].rstrip(string.digits)
                    if coltype_stripped_of_trailing_numbers not in valid_types:
                        print("[Aurora PG Handler] column type is invalid. Refer to allowed PG data types here - https://www.postgresql.org/docs/14/datatype.html")
                        return False

                if schema_as_dict[col][1].upper() not in valid_constraints:
                    print("""[Aurora PG Handler] column constraint is invalid. If you intended to have no constraint for a column, leave it empty (not space or tab; just empty).
                          Due to its complexity of auto generating DDL, EXCLUDE USING constraint is not possible using this approach. 
                          Otherwise, refer to allowed PG constraints here - https://www.postgresql.org/docs/14/ddl-constraints.html""")
                    return False

            return True
        except Exception as e:
            print(f"[Aurora PG Handler] Unable to validate types in schema file. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Unable to validate types in schema file")
    
    def run_validation(self):
        validation_results = (self._schema_file_exists_and_not_empty(), self._validate_schema_file_extension(), self._validate_schema_file_syntax(), self._validate_col_name_type_and_constraints_in_schema_file())
        return all(validation_results)
    
class AuroraPGDBHandler:
    aws_session = boto3.Session(profile_name="zippy")
    
    def __init__(self, schema_file_name: str, env: str, country_code: str, zippy_db_name: str = "postgres", zippy_schema_name: str = "public"):
        self.schema_file_path = f"../schemas/{schema_file_name}" # This script is meant to be run from "terraform/scripts" directory.
        self.env = env
        self.country_code = country_code
        
        self.rds_aurora_pg_config = {
            "client": self.aws_session.client("rds-data"),
            "cluster_arn": self._get_ssm_parameter_store_value(f"/zippy/{self.env}/{self.country_code}/rds_aurora_sls_pg_cluster_arn"),
            "secret_arn": self._get_ssm_parameter_store_value(f"/zippy/{self.env}/{self.country_code}/rds_aurora_sls_pg_credentials_secret_arn"),
            "database": zippy_db_name,
            "schema": zippy_schema_name,
            "table_name": Path(schema_file_name.split("_")[-1]).stem
        }
        
    def _get_ssm_parameter_store_value(self, key: str) -> str:
        ssm_client = self.aws_session.client("ssm")
        ssm_value = ssm_client.get_parameter(Name=key, WithDecryption=False)
        return ssm_value["Parameter"]["Value"]

    def _run_query(self, query: str, batch_execute: bool = False, parameter_sets=None) -> dict:
        try:
            if not batch_execute and self.rds_aurora_pg_config["database"] is not None:
                query_execution_details = self.rds_aurora_pg_config["client"].execute_statement(
                    resourceArn=self.rds_aurora_pg_config["cluster_arn"],
                    secretArn=self.rds_aurora_pg_config["secret_arn"],
                    database=self.rds_aurora_pg_config["database"],
                    sql=query
                )
            elif batch_execute and None not in [self.rds_aurora_pg_config["database"], parameter_sets]:
                query_execution_details = self.rds_aurora_pg_config["client"].batch_execute_statement(
                    resourceArn=self.rds_aurora_pg_config["cluster_arn"],
                    secretArn=self.rds_aurora_pg_config["secret_arn"],
                    database=self.rds_aurora_pg_config["database"],
                    parameterSets=parameter_sets,
                    sql=query
                )
            else:
                query_execution_details = self.rds_aurora_pg_config["client"].execute_statement(
                    resourceArn=self.rds_aurora_pg_config["cluster_arn"],
                    secretArn=self.rds_aurora_pg_config["secret_arn"],
                    sql=query
                )
            return query_execution_details

        except Exception as e:
            print(f"[Aurora PG Handler]. Query execution has failed. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Query execution has failed")

    
    def _generate_DDL_query_for_creating_table(self) -> str:
        
        def generate_column_defn_from_dict(schema_as_dict: dict) -> str:
            q = ""
            for ix, col in enumerate(schema_as_dict):
                q+=(f"""{col}    {schema_as_dict[col][0]}""")
                
                if (schema_as_dict[col][1] != ""):
                    q+=(f"""    {schema_as_dict[col][1].upper()}""")
                
                if (ix+1 != len(schema_as_dict)):
                    q+=(f""", 
                """)
            return q
        
        schema_as_dict = {}
        try:                
            with open(self.schema_file_path, "r") as s:
                reader = csv.reader(s)
                for row in reader:
                    # Read the first column of the csv as the key, then read the remaining columns as a list for the values
                    schema_as_dict[row[0]] = [row[x] for x in range (1, len (row))]
            
            ddl_query_for_create_table = f"""
            CREATE TABLE IF NOT EXISTS {self.rds_aurora_pg_config["schema"]}.{self.rds_aurora_pg_config["table_name"]} (
                {generate_column_defn_from_dict(schema_as_dict)}
            );
            """
            
            print(f"""
DDL query from zippy codegen = 
                  
{ddl_query_for_create_table}
                  
""")
            return ddl_query_for_create_table
        except Exception as e:
            print(f"[Aurora PG Handler] Unable to generate DDL query. Reason = {str(e)}")
            raise Exception("[Aurora PG Handler] Unable to generate DDL query.")
        
    def execute(self) -> bool:
        execution_successful = False
        try:
            response = self._run_query(self._generate_DDL_query_for_creating_table())
            print(f"""{self.rds_aurora_pg_config["database"]}.{self.rds_aurora_pg_config["schema"]}.{self.rds_aurora_pg_config["table_name"]} database has been created in Aurora PG. Query response (request id) = {response["ResponseMetadata"]}""")
            execution_successful = True
        except Exception as e:
            pass
        finally:
            return execution_successful


def main():
    args = parser.parse_args()
    # Run Validators.
    validator = AuroraPGSchemaFileValidator(args.schema_file_name)
    is_valid = validator.run_validation()
    is_complete = is_valid
    if is_valid and args.create_table == "y":
        db_handler = AuroraPGDBHandler(args.schema_file_name, args.env, args.country_code)
        execute_successful = db_handler.execute()
        is_complete = is_valid & execute_successful

    # The below print statements are required; for STD OUT since these results will be interpreted in a shell script.
    if is_complete:
        print("all ok")
    else:
        print("invalid")


if __name__ == "__main__":
    main()