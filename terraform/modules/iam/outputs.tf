output "iam_arn" {
  value = join("", aws_iam_role.zippy_iam_role.*.arn)
}