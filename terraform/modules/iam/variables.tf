variable "env" {}
variable "country_code" {}
variable "aws_component" {}
variable "create" {
  default = false
}
variable "default_tags" {}