variable "env" {}
variable "country_code" {
  default = "au"
}
variable "create" {
  default = false
}
variable "lambda_name" {}
variable "package_name" {}
variable "handler_name" {
  default = "main"
}
variable "lambda_iam_arn" {}
variable "runtime" {
  default = "go1.x"
}
variable "timeout" {
  default = 60
}
variable "memory_size" {
  default = 3008 # limits = https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-limits.html
  # Ref: https://stackoverflow.com/questions/70943739/aws-lambda-memorysize-value-failed-to-satisfy-constraint
}
variable "lambda_tracing_config" {
  default = "PassThrough" // Can be PassThrough or Active
}

variable "region_of_operation" {}
variable "zippy_default_database_name" {
  sensitive   = true
}
variable "zippy_db_resource_arn" {
  sensitive   = true
}
variable "zippy_db_secret_arn" {
  sensitive   = true
}
variable "layers" {
  type    = list(any)
  default = []
}
variable "default_tags" {}