locals {
  source_code_zip_location = "lambda_source_code_zip/${var.lambda_name}.zip"
  lambda_function_name     = "zippy_${var.env}_${var.country_code}_${var.lambda_name}"
}

data "archive_file" "zippy_lambda_archive" {
  count       = var.create ? 1 : 0
  type        = "zip"
  source_dir  = "lambdas/${var.package_name}/"
  output_path = local.source_code_zip_location
}

resource "aws_lambda_function" "zippy_lambda" {
  count = var.create ? 1 : 0

  filename         = local.source_code_zip_location
  function_name    = local.lambda_function_name
  role             = var.lambda_iam_arn
  handler          = var.handler_name
  source_code_hash = var.create ? join("", data.archive_file.zippy_lambda_archive.*.output_base64sha256) : null
  runtime          = var.runtime
  timeout          = var.timeout
  memory_size      = var.memory_size
  tracing_config {
    mode = var.lambda_tracing_config // Can be PassThrough or Active
  }
  environment {
    variables = {
      ZIPPY_REGION          = var.region_of_operation,
      ZIPPY_DATABASE        = var.zippy_default_database_name,
      ZIPPY_DB_RESOURCE_ARN = var.zippy_db_resource_arn,
      ZIPPY_DB_SECRET_ARN   = var.zippy_db_secret_arn
    }
  }
  layers = var.layers
  tags   = var.default_tags
}