variable "create" {
  default = false
}
variable "lambda_layer_name" {}
variable "env" {}
variable "country_code" {}
variable "lambda_layer_package_name" {}
variable "lambda_layer_description" {}
variable "lambda_layer_compatible_runtimes" {
  type    = list(string)
  default = ["go1.x"] # max of 5 runtimes can be provided.
}