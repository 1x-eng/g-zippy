output "lambda_layer_arn_with_version" {
  value = join("", aws_lambda_layer_version.zippy_lambda_layer.*.arn)
}
output "lambda_layer_arn_without_version" {
  value = join("", aws_lambda_layer_version.zippy_lambda_layer.*.layer_arn)
}
output "lambda_layer_name" {
  value = local.lambda_layer_name
}