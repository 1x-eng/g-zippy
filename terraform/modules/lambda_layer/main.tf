locals {
  lambda_layer_source_code_zip_location = "lambda_layers_source_code_zip/${var.lambda_layer_name}.zip"
  lambda_layer_name                     = "zippy_${var.env}_${var.country_code}_${var.lambda_layer_name}"
}

data "archive_file" "zippy_lambda_layer_archive" {
  count       = var.create ? 1 : 0
  type        = "zip"
  source_dir  = "lambda_layers/${var.lambda_layer_package_name}/"
  output_path = local.lambda_layer_source_code_zip_location
}

resource "aws_lambda_layer_version" "zippy_lambda_layer" {
  count = var.create ? 1 : 0

  source_code_hash    = var.create ? join("", data.archive_file.zippy_lambda_layer_archive.*.output_base64sha256) : null
  filename            = local.lambda_layer_source_code_zip_location
  layer_name          = local.lambda_layer_name
  description         = var.lambda_layer_description
  compatible_runtimes = var.lambda_layer_compatible_runtimes
}