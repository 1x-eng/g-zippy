locals {
  es_master_password = var.create_random_password ? random_password.zippy_es_master_password.result : var.es_master_password
  domain_name        = var.create ? var.domain_name : "unknown"
}

# Get your current AWS account ID for the access policy (resource)
data "aws_caller_identity" "current" {}

resource "random_password" "zippy_es_master_password" {
  length  = var.random_password_length
  special = false
}

resource "aws_elasticsearch_domain" "zippy_elasticsearch" {
  count                 = var.create ? 1 : 0
  domain_name           = local.domain_name
  elasticsearch_version = var.elasticsearch_version

  encrypt_at_rest {
    enabled    = "true"
    kms_key_id = var.encryption_kms_key_id
  }

  cluster_config {
    instance_type            = var.instance_type
    instance_count           = "1"
    dedicated_master_enabled = "false"
    dedicated_master_count   = "0"
    zone_awareness_enabled   = "false"
  }

  # Access policy that restricts the Elasticsearch access to your public IP only
  access_policies = <<POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "es:*",
        "Principal": "*",
        "Effect": "Allow",
        "Resource": "arn:aws:es:${var.aws_region}:${data.aws_caller_identity.current.account_id}:domain/${local.domain_name}/*",
        "Condition": {
          "IpAddress": {
            "aws:SourceIp": [
              "${var.whitelist_ip_address_with_range}"
            ]
          }
        }
      }
    ]
  }
  POLICY

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
    "indices.query.bool.max_clause_count"    = "1024"
  }

  ebs_options {
    ebs_enabled = true
    volume_type = "gp2"
    volume_size = var.volume_size
  }

  snapshot_options {
    automated_snapshot_start_hour = "0"
  }

  node_to_node_encryption {
    enabled = true
  }

  domain_endpoint_options {
    enforce_https       = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

  advanced_security_options {
    enabled                        = true
    internal_user_database_enabled = true

    master_user_options {
      master_user_name     = var.es_master_username
      master_user_password = local.es_master_password
    }
  }

  # Tags
  tags = var.default_tags
}