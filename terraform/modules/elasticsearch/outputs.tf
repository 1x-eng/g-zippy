output "elasticsearch_endpoint" {
  value = join("", aws_elasticsearch_domain.zippy_elasticsearch.*.endpoint)
}

output "elasticsearch_arn" {
  value = join("", aws_elasticsearch_domain.zippy_elasticsearch.*.arn)
}

output "elasticsearch_domain_id" {
  value = join("", aws_elasticsearch_domain.zippy_elasticsearch.*.domain_id)
}

output "elasticsearch_kibana_endpoint" {
  value = join("", aws_elasticsearch_domain.zippy_elasticsearch.*.kibana_endpoint)
}

output "es_master_password" {
  value     = local.es_master_password
  sensitive = true
}