variable "create" {
  default = false
}
variable "create_random_password" {
  default = true
}
# Network
variable "whitelist_ip_address_with_range" {
  default = "0.0.0.0/0" # Ideally, should be EIP of loadbalancer, within vpc
}

#Security
variable "encryption_kms_key_id" {}

# Elasticsearch
variable "domain_name" {}
variable "elasticsearch_version" {}
variable "instance_type" {
  default = "m4.large.elasticsearch" # t2 family doesn't support encryption at rest
}
variable "volume_size" {
  default = "10"
}
variable "random_password_length" {
  default = "16"
}
variable "aws_region" {}
variable "es_master_username" {}
variable "es_master_password" {
  default = "Z1ppY_P@ssw0rd"
}

variable "default_tags" {}