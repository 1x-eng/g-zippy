
resource "aws_cloudwatch_event_rule" "zippy_cw_rule" {
  count               = var.create ? 1 : 0
  name                = var.cw_event_rule_name
  description         = var.cw_event_rule_desc
  schedule_expression = var.cw_event_rule_schedule
}

resource "aws_cloudwatch_event_target" "zippy_cw_event_target" {
  count     = var.create ? 1 : 0
  rule      = join("", aws_cloudwatch_event_rule.zippy_cw_rule.*.name)
  target_id = var.cw_event_target_id
  arn       = var.cw_event_target_arn
}

resource "aws_lambda_permission" "zippy_cw_lambda_trigger_perms" {
  count         = var.create ? 1 : 0
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = join("", aws_cloudwatch_event_rule.zippy_cw_rule.*.arn)
}