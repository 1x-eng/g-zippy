variable "create" {
  default = false
}
variable "cw_event_rule_name" {}
variable "cw_event_rule_desc" {}
variable "cw_event_rule_schedule" {} # "rate(1 minute)"
variable "cw_event_target_id" {}
variable "cw_event_target_arn" {}
variable "lambda_function_name" {}