resource "aws_kms_key" "zippy_kms_key" {
  count       = var.create ? 1 : 0
  description = var.description
  tags        = var.default_tags
}

resource "aws_kms_alias" "zippy_kms_key_alias" {
  count         = var.create ? 1 : 0
  name          = var.alias_key_name
  target_key_id = join("", aws_kms_key.zippy_kms_key.*.key_id)
}