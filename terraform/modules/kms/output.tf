output "zippy_kms_key_id" {
  value = join("", aws_kms_key.zippy_kms_key.*.key_id)
}