locals {
  name                = "zippy-${replace(basename(path.cwd), "_", "-")}"
  region              = var.region_of_operation
  tags                = var.default_tags
  create              = var.create_rds_aurora_sls_pg
  env                 = var.env
  country_code        = var.country_code
  master_username     = var.master_username
  monitoring_interval = 60
  scaling_configuration = {
    auto_pause               = true
    min_capacity             = 2
    max_capacity             = 16
    seconds_until_auto_pause = 300
    timeout_action           = "ForceApplyCapacityChange"
  }
  public_subnets   = ["10.99.0.0/24", "10.99.1.0/24", "10.99.2.0/24"]
  private_subnets  = ["10.99.3.0/24", "10.99.4.0/24", "10.99.5.0/24"]
  database_subnets = ["10.99.7.0/24", "10.99.8.0/24", "10.99.9.0/24"]
}

################################################################################
# Supporting Resources
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = local.name
  cidr = "10.99.0.0/18"

  azs              = ["${local.region}a", "${local.region}b", "${local.region}c"]
  public_subnets   = local.public_subnets
  private_subnets  = local.private_subnets
  database_subnets = local.database_subnets

  tags = local.tags
}

################################################################################
# RDS Aurora Module - PostgreSQL
################################################################################

module "aurora_postgresql" {
  count  = local.create ? 1 : 0
  source = "terraform-aws-modules/rds-aurora/aws"

  name              = local.name
  master_username   = local.master_username
  engine            = "aurora-postgresql"
  engine_mode       = "serverless"
  storage_encrypted = true

  vpc_id                = module.vpc.vpc_id
  subnets               = module.vpc.database_subnets
  create_security_group = true
  allowed_cidr_blocks   = module.vpc.private_subnets_cidr_blocks

  monitoring_interval = local.monitoring_interval

  apply_immediately   = true
  skip_final_snapshot = true

  db_subnet_group_name            = "${local.name}-aurora-pg-db"
  db_parameter_group_name         = join("", aws_db_parameter_group.zippy_aurora_rds_sls_pg.*.id)
  db_cluster_parameter_group_name = join("", aws_rds_cluster_parameter_group.zippy_aurora_rds_sls_pg.*.id)
  enable_http_endpoint            = true
  # enabled_cloudwatch_logs_exports = # NOT SUPPORTED

  scaling_configuration = local.scaling_configuration
}

resource "aws_db_parameter_group" "zippy_aurora_rds_sls_pg" {
  count = local.create ? 1 : 0

  name        = "${local.name}-parameter-group"
  family      = "aurora-postgresql10"
  description = "${local.name}-parameter-group"
  tags        = local.tags
}

resource "aws_rds_cluster_parameter_group" "zippy_aurora_rds_sls_pg" {
  count = local.create ? 1 : 0

  name        = "${local.name}-cluster-parameter-group"
  family      = "aurora-postgresql10"
  description = "${local.name}-cluster-parameter-group"
  tags        = local.tags
}

resource "aws_secretsmanager_secret" "zippy_rds_aurora_pg_credentials" {
  count                          = local.create ? 1 : 0
  name                           = "zippy_rds_aurora_pg_credentials"
  force_overwrite_replica_secret = true
  recovery_window_in_days        = 0
}

resource "aws_secretsmanager_secret_version" "zippy_rds_aurora_pg_credentials" {
  count     = local.create ? 1 : 0
  secret_id = join("", aws_secretsmanager_secret.zippy_rds_aurora_pg_credentials.*.id)
  secret_string = jsonencode({
    engine : "postgres",
    host : module.aurora_postgresql[0].cluster_endpoint,
    username : join("", module.aurora_postgresql.*.cluster_master_username),
    password : join("", module.aurora_postgresql.*.cluster_master_password),
    port : module.aurora_postgresql[0].cluster_port,
    dbClusterIdentifier : module.aurora_postgresql[0].cluster_id
    clusterArn : module.aurora_postgresql[0].cluster_arn
  })
}

module "zippy_rds_aurora_sls_pg_secret_arn" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "rds_aurora_sls_pg_credentials_secret_arn"
  ssm_parameter_description = "RDS Aurora PG (SLS) Secret ARN"
  ssm_parameter_value       = join("", aws_secretsmanager_secret_version.zippy_rds_aurora_pg_credentials.*.arn)
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_master_username" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "rds_aurora_sls_pg_master_username"
  ssm_parameter_description = "RDS Aurora PG (SLS) Master Username"
  ssm_parameter_value       = join("", module.aurora_postgresql.*.cluster_master_username)
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_master_password" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "rds_aurora_sls_pg_master_password"
  ssm_parameter_description = "RDS Aurora PG (SLS) Master Password"
  ssm_parameter_value       = join("", module.aurora_postgresql.*.cluster_master_password)
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_arn" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "rds_aurora_sls_pg_cluster_arn"
  ssm_parameter_description = "RDS Aurora PG (SLS) Cluster Identifier"
  ssm_parameter_value       = module.aurora_postgresql[0].cluster_arn
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_write_endpoint" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "rds_aurora_sls_pg_cluster_write_endpoint"
  ssm_parameter_description = "Writer endpoint for the RDS Aurora SLS PG cluster."
  ssm_parameter_value       = module.aurora_postgresql[0].cluster_endpoint
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_read_endpoint" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "postgresql_cluster_reader_endpoint"
  ssm_parameter_description = "A read-only endpoint for the RDS Aurora SLS PG cluster, automatically load-balanced across replicas"
  ssm_parameter_value       = module.aurora_postgresql[0].cluster_reader_endpoint == null || module.aurora_postgresql[0].cluster_reader_endpoint == "" ? "unknown" : module.aurora_postgresql[0].cluster_reader_endpoint
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_engine_version" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "postgresql_cluster_engine_version_actual"
  ssm_parameter_description = "The running version of the RDS Aurora SLS PG cluster database"
  ssm_parameter_value       = module.aurora_postgresql[0].cluster_engine_version_actual
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_db_name" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "postgresql_cluster_database_name"
  ssm_parameter_description = "Name for an automatically created database on RDS Aurora SLS PG cluster creation"
  ssm_parameter_value       = local.name
  default_tags              = var.default_tags
}

module "zippy_rds_aurora_sls_pg_cluster_port" {
  source = "../ssm_parameter_store"

  create                    = local.create
  env                       = local.env
  country_code              = local.country_code
  ssm_parameter_name        = "postgresql_cluster_port"
  ssm_parameter_description = "RDS Aurora SLS PG DB Port"
  ssm_parameter_value       = module.aurora_postgresql[0].cluster_port
  default_tags              = var.default_tags
}