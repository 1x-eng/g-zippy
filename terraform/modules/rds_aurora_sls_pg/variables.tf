variable "create_rds_aurora_sls_pg" {
  default = false
}
variable "env" {}
variable "country_code" {}
variable "region_of_operation" {}
variable "master_username" {}
variable "default_tags" {}
