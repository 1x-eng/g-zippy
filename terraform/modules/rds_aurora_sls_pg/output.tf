################################################################################
# RDS Aurora Module - PostgreSQL
################################################################################

# aws_db_subnet_group
output "postgresql_db_subnet_group_name" {
  description = "The db subnet group name"
  value       = join("", module.aurora_postgresql.*.db_subnet_group_name)
}

# aws_rds_cluster
output "postgresql_cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster"
  value       = join("", module.aurora_postgresql.*.cluster_arn)
}

output "postgresql_cluster_id" {
  description = "The RDS Cluster Identifier"
  value       = join("", module.aurora_postgresql.*.cluster_id)
}

output "postgresql_cluster_resource_id" {
  description = "The RDS Cluster Resource ID"
  value       = join("", module.aurora_postgresql.*.cluster_resource_id)
}

output "postgresql_cluster_members" {
  description = "List of RDS Instances that are a part of this cluster"
  value       = module.aurora_postgresql[*].cluster_members
}

output "postgresql_cluster_endpoint" {
  description = "Writer endpoint for the cluster"
  value       = join("", module.aurora_postgresql.*.cluster_endpoint)
}

output "postgresql_cluster_reader_endpoint" {
  description = "A read-only endpoint for the cluster, automatically load-balanced across replicas"
  value       = join("", module.aurora_postgresql.*.cluster_reader_endpoint)
}

output "postgresql_cluster_engine_version_actual" {
  description = "The running version of the cluster database"
  value       = join("", module.aurora_postgresql.*.cluster_engine_version_actual)
}

# database_name is not set on `aws_rds_cluster` resource if it was not specified, so can't be used in output
output "postgresql_cluster_database_name" {
  description = "Name for an automatically created database on cluster creation"
  value       = module.aurora_postgresql[*].cluster_database_name
}

output "postgresql_cluster_port" {
  description = "The database port"
  value       = join("", module.aurora_postgresql.*.cluster_port)
}

output "postgresql_cluster_master_password" {
  description = "The database master password"
  value       = join("", module.aurora_postgresql.*.cluster_master_password)
  sensitive   = true
}

output "postgresql_cluster_master_username" {
  description = "The database master username"
  value       = join("", module.aurora_postgresql.*.cluster_master_username)
  sensitive   = true
}

output "postgresql_cluster_hosted_zone_id" {
  description = "The Route53 Hosted Zone ID of the endpoint"
  value       = join("", module.aurora_postgresql.*.cluster_hosted_zone_id)
}

# aws_rds_cluster_instances
output "postgresql_cluster_instances" {
  description = "A map of cluster instances and their attributes"
  value       = module.aurora_postgresql[*].cluster_instances
}

# aws_rds_cluster_endpoint
output "postgresql_additional_cluster_endpoints" {
  description = "A map of additional cluster endpoints and their attributes"
  value       = module.aurora_postgresql[*].additional_cluster_endpoints
}

# aws_rds_cluster_role_association
output "postgresql_cluster_role_associations" {
  description = "A map of IAM roles associated with the cluster and their attributes"
  value       = module.aurora_postgresql[*].cluster_role_associations
}

# Enhanced monitoring role
output "postgresql_enhanced_monitoring_iam_role_name" {
  description = "The name of the enhanced monitoring role"
  value       = join("", module.aurora_postgresql.*.enhanced_monitoring_iam_role_name)
}

output "postgresql_enhanced_monitoring_iam_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the enhanced monitoring role"
  value       = join("", module.aurora_postgresql.*.enhanced_monitoring_iam_role_arn)
}

output "postgresql_enhanced_monitoring_iam_role_unique_id" {
  description = "Stable and unique string identifying the enhanced monitoring role"
  value       = join("", module.aurora_postgresql.*.enhanced_monitoring_iam_role_unique_id)
}

# aws_security_group
output "postgresql_security_group_id" {
  description = "The security group ID of the cluster"
  value       = join("", module.aurora_postgresql.*.security_group_id)
}

output "postgresql_secret_arn" {
  description = "ARN of the secret hosting creds for Aurora PG SLS Db"
  value       = join("", aws_secretsmanager_secret_version.zippy_rds_aurora_pg_credentials.*.arn)
}
