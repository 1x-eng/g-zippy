variable "env" {}
variable "country_code" {}
variable "ssm_parameter_name" {}
variable "create" {
  default = false
}
variable "ssm_parameter_description" {
  default = "SSM parameter; for zippy."
}
variable "ssm_parameter_type" {
  default = "String" # Can be String, StringList and SecureString.
}
variable "ssm_parameter_value" {}
variable "default_tags" {
  type    = map(any)
  default = {}
}