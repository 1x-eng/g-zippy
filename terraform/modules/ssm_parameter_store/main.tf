resource "aws_ssm_parameter" "zippy_ssm_parameter" {
  count = var.create ? 1 : 0

  name        = "/zippy/${var.env}/${var.country_code}/${var.ssm_parameter_name}"
  description = var.ssm_parameter_description
  type        = var.ssm_parameter_type # Can be String, StringList and SecureString.
  value       = var.ssm_parameter_value
  tags        = var.default_tags
}
