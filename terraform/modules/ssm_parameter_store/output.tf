output "ssm_parameter_arn" {
  value = join("", aws_ssm_parameter.zippy_ssm_parameter.*.arn)
}
output "ssm_parameter_version" {
  value = join("", aws_ssm_parameter.zippy_ssm_parameter.*.version)
}