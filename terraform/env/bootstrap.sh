#!/usr/bin/env bash

if [[ -z "$1"  || -z "$2" ]]
then
  echo "Expecting two arguments. First one being country code (au / nz / uk) and second is environment (test / sandbox / prod)."
else
  echo "AWS_PROFILE=zippy"
  COUNTRY=$1
  ENV=$2
  TERRAFORM_BUCKET=zippy-$COUNTRY-$ENV-terraform
  DYNAMO_DB_TABLE_NAME=zippy-tf-state-lock

  mkdir -p ../.state/$COUNTRY/$ENV
  mkdir -p ../.output/$COUNTRY/$ENV
  mkdir -p ../.terraform

  if [[ $COUNTRY == "au" ]]
  then
    REGION="ap-southeast-2"
  else
    echo "Unknown region. If a new country is to be supported, please ensure bootstrap.sh is made aware of it. Currently supported countries = [au]."
    exit 1
  fi

  aws s3api create-bucket \
    --acl private \
    --region $REGION \
    --create-bucket-configuration LocationConstraint=$REGION \
    --bucket $TERRAFORM_BUCKET \
    --profile zippy

  aws dynamodb create-table \
    --table-name $DYNAMO_DB_TABLE_NAME \
    --region $REGION \
    --attribute-definitions AttributeName=LockID,AttributeType=S \
    --key-schema AttributeName=LockID,KeyType=HASH \
    --billing-mode PAY_PER_REQUEST \
    --profile zippy
fi
