terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.66.0"
    }
  }
}
provider "aws" {
  region  = var.region_of_operation
  profile = "zippy"
}