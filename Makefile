.PHONY: core/build 
core/build:
	cd core && npm install

.PHONY: core/lint
core/lint:
	cd core && npm run lint

.PHONY: core/run-offline
core/run-offline:
	cd core && npm run dev

.PHONY: core/delete
core/delete:
	cd core && npm run remove-prod

.PHONY: gzippy/deploy
gzippy/deploy:
	@make guard
	@make core/build
	cd core && npm run deploy-prod
	export country=$(COUNTRY) && export env=$(ENV) && cd terraform && make tf/init && make tf/plan && make tf/apply

.PHONY: gzippy/create-gzippy-example
gzippy/create-gzippy-example:
	make core/build && make gzippy/deploy
	export country='au' && export env='prod' && cd terraform && make gzippy/example 
	make core/build && make gzippy/deploy 
	export country='au' && export env='prod' && cd terraform && make tf/init && make tf/plan && make tf/apply

export PACKAGE_NAME
export CREATE_AURORA_PG_TABLE
export COUNTRY
export ENV
.PHONY: gzippy/create-stack
gzippy/create-stack:
	@read -p "Enter the package name for this zippy stack(If you are providing a schema file for inference, the package name must be in compliance with this schema file name. eg. if schema file name is "aurora_pg_tbl_employee.csv", then the package name MUST be "employee"): " PACKAGE_NAME; \
	read -p "Does this zippy stack need the zippy codegen infer schema (provided by user) & generate golang + typescript code(y/n): " CREATE_AURORA_PG_TABLE; \
	read -p "Enter the country code for this zippy stack(eg. au=Australia): " COUNTRY; \
	read -p "Enter the environment name for this zippy stack(dev/staging/prod): " ENV; \
	$(MAKE) _create-zippy-stack

_create-zippy-stack:
	@make gzippy/guard
	@make gzippy/guard-create-stack
	cd terraform && make gzippy/create package_name=$(PACKAGE_NAME) create_aurora_pg_table=$(CREATE_AURORA_PG_TABLE) env=$(ENV) country=$(COUNTRY) && make gzippy/build package_name=$(PACKAGE_NAME) create_aurora_pg_table=$(CREATE_AURORA_PG_TABLE)
	@make gzippy/deploy

.PHONY: gzippy/delete-all
gzippy/delete-all:
	@echo "\n\033[0;35m********* YOU ARE ABOUT TO DELETE ALL THE INFRASTRUCTURE PROVISIONED BY IaC wrt gZippy *********\n"
	@echo "\033[0m";
	@read -p "Enter the country code where zippy stacks should be destroyed(eg. au=Australia): " COUNTRY; \
	read -p "Enter the environment name where zippy stacks should be destroyed(dev/staging/prod): " ENV; \
	$(MAKE) -i _delete-zippy-stack

_delete-zippy-stack:
	@make guard
	cd core && npm run remove-prod
	export country=$(COUNTRY) && export env=$(ENV) && cd terraform && make tf/init && make tf/destroy


.PHONT: gzippy/guard-create-stack
gzippy/guard-create-stack:
ifeq ($(shell test -e './terraform/schemas/aurora_pg_tbl_$(PACKAGE_NAME).csv' && echo -n yes),yes)
	@echo "\033[0;32mzippy stack $(PACKAGE_NAME) will achieve schema inference from ./terraform/schemas/aurora_pg_tbl_$(PACKAGE_NAME).csv"
	@echo "\033[0m";
else
	@echo "\033[0;31mNo file named aurora_pg_tbl_$(PACKAGE_NAME).csv inside terraform/schemas. Cannot proceed without valid package name.\n";
	@echo "\033[0m";
	@exit 1;
endif

ifeq ($(filter $(CREATE_AURORA_PG_TABLE),y n),)
	@echo "\033[0;31mInvalid value for schema inference & codegen. Can only be y/n\n";
	@echo "\033[0m";
	@exit 1;
endif 

ifeq ($(shell printf '%s' '$(COUNTRY)' | wc -c | xargs),2)
	@echo "\033[0;32mGiven country code = $(COUNTRY)"
	@echo "\033[0m";
else
	@echo "\033[0;31mPlease provide a valid country code. (eg. au = Australia)\n";
	@echo "\033[0m";
	@exit 1;
endif

ifeq ($(filter $(ENV),dev staging prod),)
	@echo "\033[0;31mInvalid environment. Can only be dev/staging/prod\n";
	@echo "\033[0m";
	@exit 1;
endif 

.PHONT: gzippy/guard
gzippy/guard:
ifndef PACKAGE_NAME
	@echo "\033[0;31m'PACKAGE_NAME' variable/argument is not defined! \n"
	@echo "\033[0m"
	@exit 1
endif
ifndef CREATE_AURORA_PG_TABLE
	@echo "\033[0;31m'CREATE_AURORA_PG_TABLE' variable/argument is not defined. 'CREATE_AURORA_PG_TABLE' = y/n is required. \n"
	@echo "\033[0m"
	@exit 1
endif
	@make guard

.PHONT: guard
guard:
ifndef COUNTRY
	@echo "\033[0;31m'COUNTRY' variable is not defined! Please export the country code where zippy stack is to be deployed. (eg: au for Australia)\n"
	@echo "\033[0m"
	@exit 1
else
ifeq ($(shell printf '%s' '$(COUNTRY)' | wc -c | xargs),2)
	@echo "\033[0;32mGiven country code = $(COUNTRY)"
	@echo "\033[0m";
else
	@echo "\033[0;31mPlease provide a valid country code. (eg. au = Australia)\n";
	@echo "\033[0m";
	@exit 1;
endif
endif
ifndef ENV
	@echo "\033[0;31m'ENV' variable is not defined! Please export the env where zippy stack is to be deployed. (dev/staging/prod)\n"
	@echo "\033[0m"
	@exit 1
else
ifeq ($(filter $(ENV),dev staging prod),)
	@echo "\033[0;31mInvalid environment. Can only be dev/staging/prod\n";
	@echo "\033[0m";
	@exit 1;
endif 
endif
	@echo "\033[0;36mConfigured to environment: '$(ENV)' in country: '$(COUNTRY)'."
	@echo "\033[0m"

AWS_CLI_OK := $(shell aws --version 2>&1)
ifeq ('$(AWS_CLI_OK)','')
    $(error zippy requires AWS CLI. Install AWS CLI from here - docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html & try again)
endif

AWS_PROFILE_OK := $(shell aws configure list-profiles | grep zippy 2>&1)
ifeq ('$(AWS_PROFILE_OK)','')
    $(error zippy requires a dedicated AWS named profile; named "zippy". Create an profile named "zippy" [with ADMIN priv.] & try again. Ref- docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html#cli-configure-profiles-create)
endif

NODEJS_OK := $(shell node --version 2>&1)
ifeq ('$(NODEJS_OK)','')
	$(error zippy requires NodeJS. Install latest nodejs from here - nodejs.org/en/download/ & try again.)
endif

NPM_OK := $(shell npm --version 2>&1)
ifeq ('$(NPM_OK)','')
	$(error zippy requires npm. Install latest npm from here - docs.npmjs.com/try-the-latest-stable-version-of-npm & try again.)
endif

TF_OK := $(shell terraform --version 2>&1)
ifeq ('$(TF_OK)','')
	$(error zippy requires Terraform. Install latest terraform from here - learn.hashicorp.com/tutorials/terraform/install-cli & try again.)
endif

PYTHON3_OK := $(shell python3 --version 2>&1)
ifeq ('$(PYTHON3_OK)','')
    $(error zippy Python3. Install Python3, do a pip install -r requirements.txt from repo root & try again)
endif

PYTHON_EXE := $(shell echo `which python`)
BOTO3_OK := $(shell $(PYTHON_EXE) -c 'import boto3' 2>&1 )
ifneq ('$(BOTO3_OK)','')
    $(error zippy is missing dependencies for codegen. Activate your python3 virtual env, do a "pip install -r requirements.txt" from repo root & try again.)
endif


